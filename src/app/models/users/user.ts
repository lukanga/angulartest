import { Subscriber } from "../subscribers/subscriber";


export class User {
    id:number;
    username:string;
    password:string;
    role:string;
    subscriber:Subscriber;
}
