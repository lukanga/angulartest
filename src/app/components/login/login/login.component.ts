import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(i)) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
    }
  }

  constructor(  private message: NzMessageService ,private fb: FormBuilder , private router:Router , private loginservice:LoginService ) {}
  

  uname = ''
  pwd = ''
  invalidLogin = false
  msg:string;

  ngOnInit(): void {

    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  checkLogin() {

    this.uname = this.validateForm.controls['username'].value;
    this.pwd   = this.validateForm.controls['password'].value;

    (this.loginservice.authenticate(this.uname, this.pwd).subscribe(
      data => {
        this.router.navigate(['policy/list-policy']);
        this.invalidLogin = false;
      },
      error => {
        this.invalidLogin = true;
        this.msg = "Problème Authentification. ";
        this.message.error( this.msg );

        this.msg = "Merci de vérifier vos identifiants";
        this.message.error( this.msg );

        this.msg= "Code Erreur : " + error.status;
        this.message.error( this.msg );

        this.msg= "Message Erreur : " +error.message;
        this.message.error( this.msg );

        console.log( error );
        this.message.error( this.msg );
      }
    )
    );

  }

}
