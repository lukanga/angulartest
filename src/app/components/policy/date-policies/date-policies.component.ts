import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fr_FR, NzI18nService } from 'ng-zorro-antd/i18n';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Currency } from 'src/app/models/currency/currency';
import { User } from 'src/app/models/users/user';
import { CurrencyService } from 'src/app/services/currency/currency.service';
import { LoginService } from 'src/app/services/login/login.service';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { ReportPolicyService } from 'src/app/services/reports/report-policy.service';

@Component({
  selector: 'app-date-policies',
  templateUrl: './date-policies.component.html',
  styleUrls: ['./date-policies.component.scss']
})
export class DatePoliciesComponent implements OnInit {

  header="Informations Devise";
  dateFormat = 'dd/MM/yyyy';
  dateListForm: FormGroup;
  sessionData:any;
  currencies:Currency [];
  current = 0;
  date1: Date;
  date2: Date;
  date1a:Number;
  date2a:Number;

  constructor( private route:Router , private currencyService:CurrencyService , private message: NzMessageService ,  private fb: FormBuilder , private userService:LoginService, private reportService:ReportPolicyService) { }

  ngOnInit(): void {
    this.sessionData = sessionStorage.getItem('username');
    this.initForms();
  }

  initForms() {
    this.dateListForm = this.fb.group({
      date_debut: ['', Validators.required],
      date_fin: ['', Validators.required ]
    });
  }

  async printFileList() {

    this.date1 = this.dateListForm.controls['date_debut'].value;
    this.date2 = this.dateListForm.controls['date_fin'].value;
    this.date1a = Math.ceil(this.date1.getTime()/1000);
		this.date2a = Math.ceil(this.date2.getTime()/1000);
    console.log("Print Response XLSX : " +this.date1a );
    console.log("Print Response XLSX : " +this.date2a );

    await this.reportService.generatePdfList('xlsx',''+this.date1a ,''+this.date2a ).subscribe(
          response  => {
            console.log("Print Response XLSX : " +response );
        const file = new Blob([response], { type: 'application/vnd.ms-excel' });
        const fileURL = URL.createObjectURL(file);
        //window.open(fileURL);
        var anchor = document.createElement("a");
        anchor.download="pardate.xlsx";
        anchor.href = fileURL;
        anchor.click();
          }
        );
  }
}
