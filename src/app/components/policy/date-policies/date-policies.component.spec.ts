import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DatePoliciesComponent } from './date-policies.component';

describe('DatePoliciesComponent', () => {
  let component: DatePoliciesComponent;
  let fixture: ComponentFixture<DatePoliciesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatePoliciesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePoliciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
