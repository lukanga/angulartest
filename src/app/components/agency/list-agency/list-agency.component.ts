import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Agency } from 'src/app/models/agency/agency';
import { Subscriber } from 'src/app/models/subscribers/subscriber';
import { User } from 'src/app/models/users/user';
import { AgencyService } from 'src/app/services/agency/agency.service';
import { LoginService } from 'src/app/services/login/login.service';
import { SubscriberService } from 'src/app/services/subscribers/subscriber.service';

@Component({
  selector: 'app-list-agency',
  templateUrl: './list-agency.component.html',
  styleUrls: ['./list-agency.component.scss']
})
export class ListAgencyComponent implements OnInit {

  sessionData:any;
  agencies:Agency[];
  subscribers:Subscriber[];
  agency: Agency;
  isVisible: boolean;
  isEditable: boolean;
  dateFormat:string="dd/MM/YYYY";

  constructor( private message:NzMessageService, private modal: NzModalService , private agencyService:AgencyService , private subscriberService:SubscriberService , private userService:LoginService ) { }

  ngOnInit(): void {
    this.sessionData = sessionStorage.getItem('username');
    this.getAgencies();
    this.getSubscribers();
  }

  getAgencies() {
    this.agencyService.getAgencies().subscribe(
      response =>{
        this.agencies = response;
      },
      error => { this.message.error("Erreur " +error.message ) }
    )
  }

  getSubscribers() {
    this.subscriberService.getSubscribers().subscribe(
      response =>{
        this.subscribers = response;
      },
      error => { this.message.error("Erreur " +error.message ) }
    );
  }

  handleOk() {
    this.isVisible=false;
    this.isEditable=false;
  }

  handleCancel(){
    this.isVisible=false;
    this.isEditable=false;
  }

  showAgency( agency:Agency ): void {
    this.isVisible = true;
    this.agency = agency;
  }



  editAgency(age:Agency) : void {
    this.isEditable=true;
    this.agency = age;
  }


  async handleOkEditAgency( agency:Agency ) {
    
    let uname:string = this.sessionData;
    
    await this.userService.getUser(uname).then(
      response=> {
        
        let u = new User();
        u.id = response.id;
        u.role=response.password;
        u.subscriber = response.subscriber
        u.username = response.username;
        
        agency.user_Updated  = u;

        this.agencyService.updateAgency( agency.id , agency ).subscribe(
            response  => {
              this.message.success("Agence modifiée avec succès!");
              this.isVisible=false;
              this.isEditable=false;
              this.getAgencies();
            }, error  => { console.log(error) }
        );

      }
    )
    
  }


  showDeleteConfirm(agency:Agency): void {
    this.modal.confirm({
      nzTitle: 'Etes-vous sûr de supprimer cet élément ?',
      nzContent: '<b style="color: red;"> Devise : </b>' 
                  + agency.name+  
                  ''+
                  '<p> <b>Souscripteur : </b> ' 
                  + agency.subscriber.name +  
                  '</p>'+
                  '<p> <b>Cr&eacute;&eacute; Le : </b> ' 
                  + agency.createdAt + 
                  '</p>',

      nzOkText: 'Oui',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.deleteAgency(agency),
      nzCancelText: 'Non',
      nzOnCancel: () => console.log('Cancel')
    });
  }


  deleteAgency( agenc:Agency) {
    this.agencyService.deleteAgency( agenc.id , agenc ).subscribe(
      response  => {
        this.message.error("Agence supprimée avec succès!");
        this.getAgencies();
      }
    )
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

}
